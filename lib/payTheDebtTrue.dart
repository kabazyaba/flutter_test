import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/myAccounts.dart';







class PayTheDebtTrue extends StatelessWidget{


  final int _index;

  PayTheDebtTrue(this._index);






  @override
  Widget build(BuildContext context) {

    var isPayment;

    if(_index > 500){
      isPayment = paymentAccepted();
    }else
    {
      isPayment = paymentFailed();
    }





    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          iconTheme: IconThemeData(color: Colors.black),
          title: new Text(
            'Погасити заборгованність',
            style: new TextStyle(color: Colors.black),
          ),
        ),
        body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Column(
                ),
                new Center(
                  child: isPayment
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    new Container(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Padding(
                            padding: new EdgeInsets.all(50.0),
                            child: new SizedBox(
                              width: 250.0,
                              height: 48.0,
                              child: new RaisedButton(
                                onPressed: ()=> Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyAccounts())),
                                shape: new RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(30.0)),
                                disabledColor: Color(0xFFeddfa3),
                                disabledTextColor: Colors.grey,
                                textColor: Colors.black,
                                color: Colors.yellow,
                                child: new Text('ПРОДОВЖИТИ',
                                    style: new TextStyle(
                                      fontSize: 14.0, )),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )
              ],
            )
        ));
  }

  paymentAccepted(){

    return new Column(
      children: <Widget>[
        new Icon(Icons.check,
          size: 120.0,
          color: Color(0xFF51BF48),),
        new Container(
            child:
            new Text('Ви успішно погасили кредит',
              textAlign: TextAlign.center,
              style: new TextStyle(fontSize: 16.0),)
        ),
        new Container(
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text('на сумму ',
                  textAlign: TextAlign.center,
                  style: new TextStyle(fontSize: 16.0)
              ),
              new Text('$_index ₴',
                  textAlign: TextAlign.center,
                  style: new TextStyle(fontSize: 16.0,color: Color(0xFF51BF48),fontWeight: FontWeight.bold)
              ),
            ],
          ),
        ),
      ],
    );
  }

  paymentFailed(){

    return new Column(
      children: <Widget>[
        new Icon(Icons.close,
          size: 120.0,
          color: Color(0xFFE31616),),
        new Container(
            child:
            new Text('Недостатньо коштів на рахунку',
              textAlign: TextAlign.center,
              style: new TextStyle(fontSize: 16.0),)
        ),
        new Container(
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text('*5678',
                  textAlign: TextAlign.center,
                  style: new TextStyle(fontSize: 16.0,fontWeight: FontWeight.bold)
              ),
            ],
          ),
        ),

      ],
    );
  }

}


