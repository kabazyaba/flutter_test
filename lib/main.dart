import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_groshi/autorizationScreen.dart';
import 'package:flutter_app_groshi/creditCalculator.dart';
import 'package:flutter_app_groshi/myAccounts.dart';
import 'package:flutter_app_groshi/registrationScreen.dart';

void main() => runApp(
      new MyApp(),
    );

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    print('I build new MaterialApp');
    return new MaterialApp(
        home: new HomeScreen(),
        routes: <String, WidgetBuilder> {
          '/home':(BuildContext context) => MyApp(),
          '/authorizationScreen': (BuildContext context) => new AuthorizationScreen(),
          '/registrationScreen': (BuildContext context) => new RegistrationScreen(),
          '/myAccounts': (BuildContext context) => new MyAccounts(),
          '/creditCalculator': (BuildContext context) => new CreditCalc(),

        }
      );
  }
}

BuildContext _context;

class HomeScreen extends StatelessWidget {
  final _sizeTextWhite = const TextStyle(fontSize: 14.0, color: Colors.white);
  final _sizeTextWhiteSmall =
      const TextStyle(fontSize: 12.0, color: Colors.white70);
  final _sizeTextBlue =
      const TextStyle(fontSize: 14.0, color: Color(0xFF24BBEC));



  Future<bool> _exitCheck(BuildContext context) async {
    print('I build dialog');
    return showDialog(
      context: context,
      child: new AlertDialog(
        title: new Text('Закрити застосунок'),
        content: new Text('Ви дійсно вирішили вийти?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => exit(0),
            child: new Text('Так'),
          ),
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('Ні, я передумав(ла)'),
          ),
        ],
      ),
    )
        ??
        false
    ;

  }




  @override
  Widget build(BuildContext context) {
    print('I build new HomeScreen');

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
//      DeviceOrientation.portraitDown,
    ]);


    _context = context;
    return
      new WillPopScope (

        onWillPop:() => _exitCheck(context),
        child:

        Scaffold(
        backgroundColor: Colors.lightBlue,
        body:
          new Stack(
              children: <Widget>[
            new Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Column(
                    children: <Widget>[
                      new Container(
                        height: MediaQuery.of(context).size.height*0.7,
                          decoration: new BoxDecoration(
                              image: new DecorationImage(
                                image: new AssetImage('res/img/groshi.png'),
                                fit: BoxFit.fitHeight,
                              ))),
                    ],
                  ),
                  new Column(
                    children: <Widget>[
                      new Padding(padding: EdgeInsets.only(top: 20.0),
                      child: new SizedBox(
                        width: 219.0,
                        height: 48.0,
                        child: new OutlineButton(
                          onPressed:() {Navigator.pushNamed(context, '/authorizationScreen');},
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          child: new Text(
                            'УВІЙТИ',
                            style: _sizeTextWhite,
                          ),
                        ),
                      ),
                      ),
                      new Padding(padding: EdgeInsets.only(top: 16.0),
                      child: new SizedBox(
                        width: 219.0,
                        height: 48.0,
                        child: new RaisedButton(
                          color: Colors.white,
                          splashColor: Colors.white,
                          onPressed: () {Navigator.pushNamed(context, '/registrationScreen');},
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          child: new Text(
                            'ЗАРЕЄСТРУВАТИСЬ',
                            style: _sizeTextBlue,
                          ),
                        ),
                      ),
                      ),


                      new Container(
                        padding: new EdgeInsets.only(top: 23.0,bottom: 19.0),
                        child: new Text(
                          'Безкоштовна підтримка клієнтів: 0800 800 800',
                          style: _sizeTextWhiteSmall,
                        ),
                      ),
                    ],
                  )


                ]),
            new Container(
              height: MediaQuery.of(context).size.height*0.7,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
//              margin: EdgeInsets.only(top: 10.0),
              padding: new EdgeInsets.only(right: 34.0, left: 34.0),
              child: new Container(
                  alignment: Alignment.center,
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                    image: new AssetImage('res/img/logo.png'),
//                    fit: BoxFit.fill,
                  ))),
            ),
          ]
          ),
        )
    );
  }


  void registerAction() {
    Navigator.push(
      _context,
      MaterialPageRoute(builder: (context) => RegistrationScreen()),
    );
  }
}
