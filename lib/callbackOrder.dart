import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/date_time_picker/datetime_picker_formfield.dart';
import 'package:flutter_app_groshi/date_time_picker/time_picker_formfield.dart';
import 'package:flutter_app_groshi/creditFin.dart';
import 'package:intl/intl.dart' show DateFormat;


class CallbackOrder extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<CallbackOrder> {

  final myControllerDate = TextEditingController();
  final myControllerTime =TextEditingController();
  bool _textIsPresent = false;
  final dateFormat = DateFormat("d/MM/yyyy");
  final timeFormat = DateFormat("HH:mm");
  DateTime date;
  TimeOfDay time;

  void initState(){
    super.initState();
    myControllerDate.addListener(_changeButtonState);
    myControllerTime.addListener(_changeButtonState);
  }

  _changeButtonState(){
    var textDate = myControllerDate.text.length;
    var textTime = myControllerTime.text.length;



    if(textDate > 0 && textTime > 0 ){
      setState(() => _textIsPresent = true);
    }else
      setState(() => _textIsPresent =false);
  }

    DateTime _dateTime(){
    DateTime d = new DateTime(date.year, date.month, date.day, time.hour, time.minute );
    return d;
  }

  @override
  Widget build(BuildContext context) {
    var _onPressed;

    if(_textIsPresent && _dateTime().isAfter(DateTime.now())){
      _onPressed = ()=> Navigator.push(context, MaterialPageRoute(builder: (context) => CreditFin()));
    }


    return new Scaffold(

        resizeToAvoidBottomPadding: true,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.4,
          iconTheme: IconThemeData(color: Colors.black),
          title: new Text(
            'Кредитний калькулятор',
            style: new TextStyle(color: Colors.black),
          ),
        ),
        body: SingleChildScrollView(
          reverse: false,
          child: Container(
             height: MediaQuery.of(context).size.height - AppBar().preferredSize.height - 24,
                color: Colors.white,

                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Column(
                      children: <Widget>[
                      ],
                    ),
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.only(bottom: 25.0),
                          child: new Icon(
                            Icons.ring_volume,
                            size: 80.0,
                            color: Colors.cyan,
                          ),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(left: 20.0, right: 20.0),
                          child: new Text(
                            'Для уточнення деталей вам передзонить оператор call-центру у зручний для вас час',
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                                fontSize: 18.0, fontWeight: FontWeight.w500),
                          ),
                        ),
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            new SizedBox(
                              height: 35.0,
                            ),
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                new Container(
                                  width: 90.0,
                                  child: new DateTimePickerFormField(
                                    controller: myControllerDate,
                                    resetIcon: null,
                                    format: dateFormat,
                                    style: new TextStyle(color: Colors.cyan, fontSize: 16.0),
                                    decoration: InputDecoration(hintText: 'Дата',
                                        labelStyle: new TextStyle(color: Colors.grey)
                                    ),
                                    onChanged: (d) => setState(() => date = d),

                                  ),
                                ),
                                new Container(
                                  width: 90.0,
                                  child: TimePickerFormField(
                                    controller: myControllerTime,
                                    resetIcon: null,
                                    format: timeFormat,
                                    style: new TextStyle(color: Colors.cyan, fontSize: 16.0),
                                    decoration: InputDecoration(hintText: 'Час',
                                      labelStyle: new TextStyle(color: Colors.grey,),
                                    ),
                                    onChanged: (t) => setState(() => time = t),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )

                      ],
                    ),
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Container(
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              new Padding(
                                padding: new EdgeInsets.all(50.0),
                                child: new SizedBox(
                                  width: 250.0,
                                  height: 48.0,
                                  child: new RaisedButton(
                                    onPressed: _onPressed,
                                    shape: new RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(30.0)),
                                    disabledColor: Color(0xFFeddfa3),
                                    disabledTextColor: Colors.grey,
                                    textColor: Colors.black,
                                    color: Colors.yellow,
                                    child: new Text('ПЕРЕДЗВОНІТЬ МЕНІ',
                                        style: new TextStyle(
                                          fontSize: 14.0,
                                        )),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                )))
//          ],

//        )

        );
  }
}
