import 'dart:async';

import 'package:flutter/material.dart';
import './custom_keyboard.dart';
import './code_view.dart';
import 'package:vibrate/vibrate.dart';
import 'code_view.dart';

class PinCode extends StatefulWidget {
  final Text title;
  final int count;
  final Function onCodeEntered;
  final int codeLength;
  final TextStyle keyTextStyle, codeTextStyle;

  PinCode({
    this.count,
    this.title,
    this.codeLength = 6,
    this.onCodeEntered,
    this.keyTextStyle = const TextStyle(color: Colors.white, fontSize: 25.0),
    this.codeTextStyle = const TextStyle(
        color: Colors.white, fontSize: 25.0, fontWeight: FontWeight.bold),
  });

  PinCodeState createState() => PinCodeState();
}

class PinCodeState extends State<PinCode> {
  String smsCode = "";
  String _trueCode = CodeViewState().pin;


  @override
  Widget build(BuildContext context) {
    print('I build new PinCodeView');
    return Container(
      color: Theme.of(context).primaryColor,
      child: Column(children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                Expanded(child: Container()),
                Container(
                    padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 5.0),
                    child: widget.title),
                Expanded(child: Container()),
                CodeView(
                  code: smsCode,
                  length: widget.codeLength,
                ),
                Expanded(child: Container()),
              ],
            ),
          ),
        ),
        CustomKeyboard(
          textStyle: widget.keyTextStyle,
          onPressedKey: (key) {
            if (smsCode.length < widget.codeLength) {
              setState(() {
                smsCode = smsCode + key;
                CodeViewState().pinCountIncrement();
              });
            }
            if (smsCode.length == widget.codeLength) {
              if (smsCode == _trueCode) {
                Vibrate.feedback(FeedbackType.warning);
                new Timer(Duration(milliseconds: 300),(){Vibrate.feedback(FeedbackType.warning);});

                widget.onCodeEntered(smsCode);
              }
              else {
                new Timer(const Duration(milliseconds: 100), () {
                  setState(() {
                    smsCode = "";
                    CodeViewState().initState();
                    Vibrate.vibrate();
                  });

                });
              }
            }
          },
          onBackPressed: () {
            int codeLength = smsCode.length;
            if (codeLength > 0)
              setState(() {
                smsCode = smsCode.substring(0, codeLength - 1);
                CodeViewState().pinCountDecrement();
              });
          },
        ),
      ]),
    );
  }
}
