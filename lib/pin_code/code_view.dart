import 'dart:async';

import 'package:flutter/material.dart';
import 'dart:core';



class CodeView extends StatefulWidget {
  CodeView({
    this.code,
    this.length = 6,
    this.codeTextStyle,
  });

  final String code;
  final int length;
  final TextStyle codeTextStyle;

  CodeViewState createState() => CodeViewState();
}


int tryCount = 0;
int pinCount =0;

class CodeViewState extends State<CodeView> with SingleTickerProviderStateMixin {
   String pin = '1212';


  final TextEditingController textController = TextEditingController();
  AnimationController controller;



  IconData getCodeAt(index) {
    if (widget.code == null || widget.code.length < index)
      return Icons.radio_button_unchecked;
    else {
      return Icons.radio_button_checked;
    }
  }

  void initState(){
    print('I init CodeView');
    pinCount = 0;
    print("working");
    tryCount++;
    print('tryCount = $tryCount');
    controller = AnimationController(duration: const Duration(milliseconds: 200), vsync: this);
    super.initState();
  }

  void tryCountReset(){
    tryCount=0;
  }

  void pinShake(){
    controller.forward(from: 0.0);
  }

//  void countCountdown(){
//    count =0;
//  }

  void pinCountIncrement(){
    pinCount++;
    print('PinCount = $pinCount');
  }

  void pinCountDecrement(){
    pinCount--;
    print('PinCount = $pinCount');
  }

  void dispose(){
    print('I dispose CodeWiew');
    new Timer(const Duration(milliseconds: 20),(){
//      controller.dispose();
      tryCount = 0;
    });
    super.dispose();
  }



  _getWidget(){
    if(tryCount<4){
      if(widget.code==pin){
        print('I kill new GetPinWidget Tiker');
        controller.dispose();
        return _getCodeViews();
      }else{
        print('I returm RadioIcon in new GetPinWidget');
        return _getCodeViews();
      }
    }else{
      print('I return to Home');
        new Timer(const Duration(milliseconds: 0),(){
          tryCount = 0;
          Navigator.pop(context);
        });



      return _getPinMessage();
    }
  }


  _getPinMessage(){
      return  new Container(
          margin: EdgeInsets.all(5.0),
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: new Text('спроби закінчилися}',
          style: new TextStyle(color: Colors.white),
        ),
    );
  }



  _getCodeViews() {
    List<Widget> widgets = [];
    for (var i = 0; i < widget.length; i++) {
      print('I build new RadioIcon');
      widgets.add(
        Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(5.0),
              padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
              child: Icon(
                getCodeAt(i + 1),
                color: Colors.white,
                size: 20.0,
              ),
            ),
          ],
        ),
      );
    }
    return new Row(
      children: widgets,
    );
  }

  @override
  Widget build(BuildContext context) {
    print('I build new CodeWiew');


    if(pinCount == 4){
        controller.forward(from: 0.0);
    }
    final Animation<double> offsetAnimation =
    Tween(begin: 0.0, end: 24.0).chain(CurveTween(curve: Curves.elasticIn)).animate(controller)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        }
      });
    return Column(
      children: <Widget>[
        Wrap(
          children:<Widget>[
            AnimatedBuilder(
                animation: offsetAnimation,
                builder: (buildContext, child) {
//                if (offsetAnimation.value < 0.0) print('${offsetAnimation.value + 8.0}');
                  return Container(
                    margin: EdgeInsets.symmetric(horizontal: 24.0),
                    padding: EdgeInsets.only(left: offsetAnimation.value + 24.0, right: 24.0 - offsetAnimation.value),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        _getWidget()
                      ],
                    ),
                  );
                }),


        ]
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: new Text('спроб залишилось ${4-tryCount}',
          style: new TextStyle(color: Colors.white),
          ),
        )
      ],
    );
  }
}
