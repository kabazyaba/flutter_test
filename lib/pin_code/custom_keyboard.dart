import 'package:flutter/material.dart';
import 'package:vibrate/vibrate.dart';

class CustomKeyboard extends StatefulWidget {
  final Function onBackPressed, onPressedKey;
  final TextStyle textStyle;
  CustomKeyboard({
    this.onBackPressed,
    this.onPressedKey,
    this.textStyle,
  });

  CustomKeyboardState createState() => CustomKeyboardState();
}

class CustomKeyboardState extends State<CustomKeyboard> {
  String code = "";

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FloatingActionButton(
                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: () => {
                  widget.onPressedKey("1"):
                  Vibrate.feedback(FeedbackType.success)
                  },
                child: Text(
                  "1",
                  style: widget.textStyle,
                ),
              ),
              FloatingActionButton(
                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: () => {
                  widget.onPressedKey("2"):
                  Vibrate.feedback(FeedbackType.success)
                },
                child: Text(
                  "2",
                  style: widget.textStyle,
                ),
              ),
              FloatingActionButton(
                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: () => {
                  widget.onPressedKey("3"):
                  Vibrate.feedback(FeedbackType.success)
                },
                child: Text(
                  "3",
                  style: widget.textStyle,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FloatingActionButton(
                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: () => {
                  widget.onPressedKey("4"):
                  Vibrate.feedback(FeedbackType.success)
                },
                child: Text(
                  "4",
                  style: widget.textStyle,
                ),
              ),
              FloatingActionButton(
                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: () => {
                  widget.onPressedKey("5"):
                  Vibrate.feedback(FeedbackType.success)
                },
                child: Text(
                  "5",
                  style: widget.textStyle,
                ),
              ),
              FloatingActionButton(
                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: () => {
                  widget.onPressedKey("6"):
                  Vibrate.feedback(FeedbackType.success)
                },
                child: Text(
                  "6",
                  style: widget.textStyle,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FloatingActionButton(
                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: () => {
                  widget.onPressedKey("7"):
                  Vibrate.feedback(FeedbackType.success)
                },
                child: Text(
                  "7",
                  style: widget.textStyle,
                ),
              ),
              FloatingActionButton(
                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: () => {
                  widget.onPressedKey("8"):
                  Vibrate.feedback(FeedbackType.success)
                },
                child: Text(
                  "8",
                  style: widget.textStyle,
                ),
              ),
              FloatingActionButton(

                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: () => {
                  widget.onPressedKey("9"):
                  Vibrate.feedback(FeedbackType.success)
                },
                child: Text(
                  "9",
                  style: widget.textStyle,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FloatingActionButton(
                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: null,
                child: Text(
                  "",
                  style: widget.textStyle,
                ),
              ),
              FloatingActionButton(
                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: () => {
                  widget.onPressedKey("0"):
                  Vibrate.feedback(FeedbackType.success)
                },
                child: Text(
                  "0",
                  style: widget.textStyle,
                ),
              ),
              FloatingActionButton(
                heroTag: null,
                foregroundColor: Colors.black,
                elevation: 0.0,
                onPressed: () => {
                  widget.onBackPressed():
                  Vibrate.feedback(FeedbackType.success)
                },
                child: Icon(
                  Icons.backspace,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
