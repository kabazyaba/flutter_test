import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/account.dart';
import 'package:flutter_app_groshi/dialogs.dart';

class AccountDetails extends StatelessWidget {



  final int _itemIndex;

  AccountDetails(this._itemIndex);

  final List<Account> _allAccounts = Account.allAccounts();

  final _sizeTextBlack = const TextStyle(fontSize: 20.0, color: Colors.black);
//  BuildContext _context;


  @override
  Widget build(BuildContext context) {


    double width = MediaQuery.of(context).size.width;

    return new Scaffold(
      body: DefaultTabController(
        length: 2,
        child: Center(
          child: NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(

                  iconTheme: IconThemeData(color: Colors.black),
                  backgroundColor: Colors.white,
                  expandedHeight: 380.0,
                  floating: false,
                  title: Text(_allAccounts[_itemIndex].itemName,
                      style: _sizeTextBlack),
                  pinned: true,
                  elevation: 0.0,
                  flexibleSpace: FlexibleSpaceBar(
                  background: new Column(
                        children:<Widget>[
                          new Padding(padding: EdgeInsets.fromLTRB(0.0, 65.0, 0.0, 20.0),
                          child: _getItemUI(context, _itemIndex),
                          ),
                        ]
                    ),
                  ),
                  bottom: new PreferredSize(
                    child: new Container(
                      decoration: new BoxDecoration(color: Colors.white),
                      child: new Column(
                          children:<Widget>[
                            new Row(
                              children: <Widget>[
                                new Column(
                                  children: <Widget>[
                                    new Container(
                                        width: width*0.7,
                                        height: 100.0,
                                        child: new Padding(padding: EdgeInsets.fromLTRB(16.0, 0.0, 0.0, 0.0),
                                          child:new Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              new Text('Рекомендований платіж',

                                              ),

                                              new Text('800.00 ₴',
                                                  style: new TextStyle(fontSize: 20.0, color: Colors.lightGreen, fontWeight: FontWeight.bold)),
                                              new Text('до 21 листопада'),
                                            ],
                                          ),
                                        )
                                    )
                                  ],
                                ),
                                new Row(
                                  children: <Widget>[
                                    new Container(
                                        width: width*0.3,
                                        child: new Padding(padding: EdgeInsets.fromLTRB(0.0, 0.0, 16.0, 0.0),
                                            child: new Column(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: <Widget>[
                                                new FloatingActionButton(
                                                    elevation: 0.0,
                                                    child: new Icon(Icons.arrow_forward, color: Colors.black),
                                                    backgroundColor:  Color(0xFFFFDD00),
                                                  onPressed: () => Dialogs().information(context),
                                                ),
                                              ],
                                            )
                                        )
                                    )

                                  ],
                                )
                              ],
                            ),
                            new SizedBox(
                              height: 3.0,
                              child: new Center(
                                child: new Container(
                                  margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
                                  height: 3.0,
                                  color: Colors.black12,
                                ),
                              ),
                            )
                          ]
                      ),
              ),
                      preferredSize: new Size(width, 110.0)),


                )
              ];
            },
            body: new Container(
              child:  getHomePageBody(context),
              )




            ),
          ),
        ),
      );
  }


  getHomePageBody(BuildContext context) {
     return ListView.builder(
          itemCount: _allAccounts.length,
          itemBuilder: _getDetailsUI,
          padding: EdgeInsets.all(0.0),
    );

  }


  Widget _getDetailsUI(BuildContext context, int index){


    double width = MediaQuery.of(context).size.width;


    return new Column(
        children:<Widget>[

          new Row(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  new Container(
                      width: width*0.7,
                      height: 100.0,
                      child: new Padding(padding: EdgeInsets.fromLTRB(16.0, 0.0, 0.0, 0.0),
                        child:new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(' + 800.00 ₴',
                                style: new TextStyle(fontSize: 20.0, color: Colors.lightGreen, fontWeight: FontWeight.bold)),
                          ],
                        ),
                      )
                  )
                ],
              ),
              new Row(
                children: <Widget>[
                  new Container(
                      width: width*0.3,
                      child: new Padding(padding: EdgeInsets.fromLTRB(0.0, 0.0, 16.0, 0.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              new Text('Сiчень 21, 19:45')
                            ],
                          )
                      )
                  )
                ],
              )
            ],
          ),
          new SizedBox(
            height: 10.0,
            child: new Center(
              child: new Container(
                margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
                height: 3.0,
                color: Colors.black12,
              ),
            ),
          )
        ]
    );
  }




  Widget _getItemUI(BuildContext context, int index) {
    return new Card(
        color: _allAccounts[index].cardColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        margin: EdgeInsets.only(left: 16.0,top:16.0, right: 16.0),
        child: new Container(
            height: 200.0,
            width: 328.0,
            child: Column(
              children: <Widget>[
                new Container(
                  padding:EdgeInsets.all( 16.0),
                  height: 60.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new Image.asset(
                        "res/img/" + _allAccounts[index].bankIcon,
                        fit: BoxFit.cover,
                        width: 130.0,
                      ),
                    ],
                  ),
                ),
                new Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top:18.0,left: 16.0,right: 16.0),
                    child: new Text(_allAccounts[index].money,
                      style: new TextStyle(color: Colors.white,fontSize: 36.0, fontWeight: FontWeight.w300 ),
                    )
                ),
                new Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(top: 5.0),
                  child: new Text('БАЛАНС',
                    style: new TextStyle( color: Colors.black26, fontSize: 14.0, fontWeight: FontWeight.w500),),
                ),

                new Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(top:15.0, left: 16.0, right: 16.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Container(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text('Початок',
                                textAlign: TextAlign.left,style: new TextStyle(fontSize: 12.0),
                              ),
                              new Container(
                                padding: EdgeInsets.only(top:0.0),
                                child: new Text(_allAccounts[index].startDate,
                                  style: new TextStyle(color: Colors.white,fontSize: 14.0, fontWeight: FontWeight.normal ),
                                ),
                              ),


                            ],
                          ),
                        ),

                        new Container(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text('Кінець',
                                textAlign: TextAlign.left, style: new TextStyle(fontSize: 12.0),
                              ),
                              new Container(
                                padding: EdgeInsets.only(top:0.0),
                                child: new Text(_allAccounts[index].stopDate,
                                  style: new TextStyle(color: Colors.white,fontSize: 14.0, fontWeight: FontWeight.normal ),
                                ),
                              ),
                          ],
                        )
                        ),




                      ]
                  ),
                ),
              ],
            )));
  }



}