import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/keyboardAction.dart';
import 'package:flutter_app_groshi/test.dart';
import 'package:flutter_app_groshi/pin_code/pin_code_view.dart';

BuildContext _context;

class AuthorizationScreen extends StatefulWidget {
  @override
  _AuthorizationScreenState createState() => _AuthorizationScreenState();
}

class _AuthorizationScreenState extends State<AuthorizationScreen> {
  final _sizeTextAmber = const TextStyle(
      fontSize: 18.0, fontStyle: FontStyle.normal, color: Colors.amber);
  String trueCode = "1212";


  @override
  Widget build(BuildContext context) {
    print('I build new AuthorizationScr');
    _context = context;

    return new
//    WillPopScope(
//        onWillPop: () async {
//          return  Navigator.pop(context, MaterialPageRoute(builder: (context) => MyApp()));
//        },
//        child: new

        Scaffold(
          resizeToAvoidBottomPadding: false,
          backgroundColor: Colors.lightBlue,
          appBar: AppBar(
            elevation: 3.0,
            centerTitle: true,
            title: Text("+380(99)123-45-67"),
            bottom: PreferredSize(
                child: Container(
                    padding: const EdgeInsets.only(
                        left: 0.0, right: 0.0, bottom: 22.0),
                    child: new SizedBox(
                      height: 18.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              correctionAction();
                            },
                            child: Text(
                              'ЗМІНИТИ',
                              style: _sizeTextAmber,
                            ),
                          ),
                        ],
                      ),
                    )),
                preferredSize: const Size.fromHeight(45.0)),
          ),
          body: PinCode(
            title: Text(
              "Введіть свій PIN",
              style: TextStyle(fontSize: 20.0, color: Colors.white),
            ),
            codeLength: 4,
            onCodeEntered: (code) {
              Navigator.of(context).pushNamedAndRemoveUntil('/myAccounts', ModalRoute.withName('/home'));

                print(code);

            },
          ),
//        )
    );
  }

  void numericKeyboardAction(String s) {
    String _screenTitle = s;
    Navigator.push(
      _context,
      MaterialPageRoute(builder: (context) => KeyboardAction(_screenTitle)),
    );
  }

  void correctionAction() {
    Navigator.push(
      _context,
      MaterialPageRoute(builder: (context) => MyFormPage()),
    );
  }
}
