import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/account.dart';
import 'package:flutter_app_groshi/accountDetails.dart';
import 'package:flutter_app_groshi/creditCalculator.dart';
import 'package:flutter_app_groshi/main.dart';
import 'package:flutter_app_groshi/moneyTransfer.dart';
import 'package:flutter_app_groshi/unicorndial/unicorndial.dart';



class MyAccounts extends StatefulWidget {
  _MyAccounts createState() => _MyAccounts();
}

class _MyAccounts extends State<MyAccounts> {



  static const List<String> icons = const ['res/img/money_transfer.png', 'res/img/add_card.png', 'res/img/new_loan.png'];


  final List<Account> _allAccounts = Account.allAccounts();

  final _sizeTextBlack = const TextStyle(fontSize: 20.0, color: Colors.black);
  BuildContext _context;


  Widget build(BuildContext context) {


    var childButtons = List<UnicornButton>();

    childButtons.add(UnicornButton(
        hasLabel: true,
        labelText: "Грошовий переказ",
        labelColor: Colors.white,
        labelBackgroundColor: Colors.black,
        currentButton: FloatingActionButton(
          heroTag: "train",
          backgroundColor: Color(0xFFFFDD00),
          mini: true,
          child: new Image.asset("res/img/money_transfer.png", width: 24.0, color: Colors.black),
          onPressed: ()=> Navigator.push(context, MaterialPageRoute(builder: (context) => MoneyTransfer())),
        )));

    childButtons.add(UnicornButton(
        hasLabel: true,
        labelText: "Додати картку",
        labelColor: Colors.white,
        labelBackgroundColor: Colors.black,
        currentButton: FloatingActionButton(
          heroTag: "airplane",
          backgroundColor: Color(0xFFFFDD00),
          mini: true,
          child: new Image.asset("res/img/add_card.png", width: 24.0, color: Colors.black),
          onPressed: () {},
        )));

    childButtons.add(UnicornButton(
        hasLabel: true,
        labelText: "Оформити новий кредит",
        labelColor: Colors.white,
        labelBackgroundColor: Colors.black,
        currentButton: FloatingActionButton(
          heroTag: "directions",
          backgroundColor: Color(0xFFFFDD00),
          mini: true,
          child: new Image.asset("res/img/new_loan.png", width: 24.0, color: Colors.black),
          onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => CreditCalc())),
        )));

    _context = context;

    return new WillPopScope(
        onWillPop: () async {
          print("Ireturn to Home from MyAccounts");
          return Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> new HomeScreen()));
        },


      child: Scaffold(

      floatingActionButton: UnicornDialer(
          backgroundColor: Color.fromRGBO(255, 255, 255, 0.6),
          parentButtonBackground: Color(0xFFFFDD00),
          orientation: UnicornOrientation.VERTICAL,
          parentButton: Icon(Icons.add, color: Colors.black),
          childButtons: childButtons),


      body: DefaultTabController(
        length: 2,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                iconTheme: IconThemeData(color: Colors.black),
                backgroundColor: Colors.white,
                expandedHeight: 56.0,
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  title: Text("Мої рахунки",
                      style: _sizeTextBlack),
                ),
              ),
            ];
          },
          body: new Container(
            color: Colors.white,
            child:new Padding(
                padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                child: getHomePageBody(context)),
          )

        ),
      ),
      drawer: new Drawer(
          child: new ListView(
            children: <Widget>[
              new DrawerHeader(
                child: new Text('Header'),
              ),
              new ListTile(
                title: new Text('First Menu Item'),
                onTap: () {},
              ),
              new ListTile(
                title: new Text('Second Menu Item'),
                onTap: () {},
              ),
              new Divider(),
              new ListTile(
                title: new Text('About'),
                onTap: () {},
              ),
            ],
          )),


      )
    );
  }




  getHomePageBody(BuildContext context ) {

    return ListView.builder(
      itemCount: _allAccounts.length,
      itemBuilder: _getItemUI,
      padding: EdgeInsets.all(0.0),
    );

  }

  Widget _getItemUI(BuildContext context, int index) {
    print('I generate GetItem');

    return new Column(
          children: <Widget>[
            Card(
                color: _allAccounts[index].cardColor,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                elevation: 3.0,
                margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                child: InkWell(
                  splashColor: Colors.lightBlueAccent,
                  onTap: (){numericKeyboardAction(index);
                  print('tap');
                  },
                  child: new Container(
                    decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                      height: 200.0,
                      width: 328.0,
                      child: Column(
                        children: <Widget>[
                          new Container(
                            padding:EdgeInsets.all( 16.0),
                            height: 60.0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Image.asset(
                                  "res/img/" + _allAccounts[index].bankIcon,
                                  fit: BoxFit.cover,
                                  width: 130.0,
                                ),
                              ],
                            ),
                          ),
                          new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(top:12.0,left: 16.0),
                              child: new Text(_allAccounts[index].accountNumber,
                                style: new TextStyle(color: Colors.white,fontSize: 24.0, fontWeight: FontWeight.normal, letterSpacing: 1.0),
                              )
                          ),
                          new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(top:12.0,left: 16.0),
                              child: new Text("Термін дії",
                                style: new TextStyle(color: Colors.white,fontSize: 12.0, fontWeight: FontWeight.normal ),
                              )
                          ),
                          new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(top:2.0,left: 16.0),
                              child: new Text(_allAccounts[index].dateExpired,
                                style: new TextStyle(color: Colors.white,fontSize: 16.0, fontWeight: FontWeight.w500 ),
                              )
                          ),
                          new Container(
                            alignment: Alignment.topLeft,
                            padding: EdgeInsets.only(left: 16.0),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Text(_allAccounts[index].holderName,
                                    style: new TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.w500 ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(right: 16.0, bottom: 16.0),
                                    child: new Image.asset(
                                      "res/img/" + _allAccounts[index].cardIcon,
                                      fit: BoxFit.cover,
                                      width: 60.0,
                                    ),
                                  )
                                ]
                            ),
                          ),
                        ],
                      )),
                )




                )
          ],

    );
  }
  void numericKeyboardAction(int index){
    Navigator.push(
      _context,
      MaterialPageRoute(builder: (context) => AccountDetails(index)),
    );
  }

}