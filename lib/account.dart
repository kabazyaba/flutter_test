import 'dart:ui';

import 'package:flutter/material.dart';

class Account {

  //--start date
  final String startDate;

  //--stop date
  final String stopDate;

  //-- money
  final String money;

  //-- index
  final String itemName;

  //-- icon
  final String bankIcon;

  //--- account number
  final String accountNumber;

  //---card color
  final Color cardColor;

  //--- holder name number
  final String holderName;

  //--- date expired
  final String dateExpired;

  //--- card icon
  final String cardIcon;

  Account({this.startDate, this.stopDate, this.money, this.itemName, this.bankIcon, this.cardColor, this.holderName, this.accountNumber, this.cardIcon, this.dateExpired});

  static List<Account> allAccounts() {

    var lstOfAccounts = new List<Account>();

    lstOfAccounts.add(new Account(stopDate: 'Stop Date', startDate:'Start Date', money: '-850.00', itemName: 'Simple Money', cardColor: Colors.white54, bankIcon: "simple_money_logo.png", holderName: "FirstName SecondName", accountNumber: "1234 5678 9123 4567",cardIcon: "visa_white_logo.png", dateExpired: "01/21"));
    lstOfAccounts.add(new Account(stopDate: 'Stop Date', startDate:'Start Date', money: '2930.00',itemName: 'AlfaBank',cardColor: Colors.red, bankIcon: "alfa_bank_logo.png", holderName: "FirstName SecondName", accountNumber: "1234 5678 9123 4567",cardIcon: "mastercard_white_logo.png", dateExpired: "06/22"));
    lstOfAccounts.add(new Account(stopDate: 'Stop Date', startDate:'Start Date', money: '-50.00',itemName: 'OTP Bank',cardColor: Colors.green, bankIcon: "otp_bank_logo.png", holderName: "FirstName SecondName", accountNumber: "1234 5678 9123 4567",cardIcon: "visa_white_logo.png", dateExpired: "11/19"));
    lstOfAccounts.add(new Account(stopDate: 'Stop Date', startDate:'Start Date', money: '-1850.00',itemName: 'Simple Money',cardColor: Colors.white54,bankIcon: "simple_money_logo.png", holderName: "FirstName SecondName", accountNumber: "1234 5678 9123 4567",cardIcon: "visa_white_logo.png", dateExpired: "09/25"));
    lstOfAccounts.add(new Account(stopDate: 'Stop Date', startDate:'Start Date', money: '3430.00',itemName: 'OTP Bank',cardColor: Colors.green, bankIcon: "otp_bank_logo.png", holderName: "FirstName SecondName", accountNumber: "1234 5678 9123 4567",cardIcon: "visa_white_logo.png", dateExpired: "11/19"));



    return lstOfAccounts;
  }
}