import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/moneyTransferMyCards.dart';
import 'package:flutter_app_groshi/moneyTransferOtherCard.dart';

class MoneyTransfer extends StatefulWidget {
  @override
  _MoneyTransferState createState() => _MoneyTransferState();
}

class _MoneyTransferState extends State<MoneyTransfer> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          iconTheme: IconThemeData(color: Colors.black),
          title: new Text(
            'Переказ коштів',
            style: new TextStyle(color: Colors.black),
          ),
        ),
        body: Container(
            decoration: new BoxDecoration(color: Colors.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Column(
                  children: <Widget>[
                    new Divider(color: Colors.grey),
                    Column(
                      children: <Widget>[
                        new Container(
                          decoration: BoxDecoration(color: Colors.white),
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Row(children: <Widget>[
                                new Container(
                                    height: 190.0,
                                    width: MediaQuery.of(context).size.width,
                                    decoration:
                                        BoxDecoration(color: Colors.white),
                                    child: Column(
                                      children: <Widget>[
                                        new Material(
                                            color: Colors.transparent,
                                            child: InkWell(
                                              child: new Padding(
                                                padding: EdgeInsets.only(
                                                    left: 18.0,
                                                    right: 18.0,
                                                    top: 10.0,
                                                    bottom: 10.0),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 10.0,
                                                          bottom: 10.0),
                                                  child: new Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      new Container(
                                                        child: new Text(
                                                          'Між своїми рахунками',
                                                          style: new TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 18.0),
                                                        ),
                                                      ),
                                                      new Container(
                                                        child: new Icon(
                                                          Icons.chevron_right,
                                                          color: Colors.blue,
                                                          size: 35.0,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            MoneyTransferMyCard()));
                                                print('tap');
                                              },
                                              splashColor: Colors.lightBlueAccent,
                                            )),
                                        new Material(
                                            color: Colors.transparent,
                                            child: InkWell(
                                              child: new Padding(
                                                padding: EdgeInsets.only(
                                                    left: 18.0,
                                                    right: 18.0,
                                                    top: 10.0,
                                                    bottom: 10.0),
                                                child: Padding(
                                                  padding:
                                                  const EdgeInsets.only(
                                                      top: 10.0,
                                                      bottom: 10.0),
                                                  child: new Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                    children: <Widget>[
                                                      new Container(
                                                        child: new Text(
                                                          'На інший рахунок',
                                                          style: new TextStyle(
                                                              color:
                                                              Colors.black,
                                                              fontSize: 18.0),
                                                        ),
                                                      ),
                                                      new Container(
                                                        child: new Icon(
                                                          Icons.chevron_right,
                                                          color: Colors.blue,
                                                          size: 35.0,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            MoneyTransferOtherCard()));
                                                print('tap');
                                              },
                                              splashColor: Colors.lightBlueAccent,
                                            )),
                                      ],
                                    )),
                              ]),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            )));
  }
}
