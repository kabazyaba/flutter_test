import 'package:flutter/material.dart';


class KeyboardAction extends StatelessWidget {

  String _screenTitle;

  KeyboardAction(String text){
    _screenTitle = text;
  }

  final _sizeTextWhite = const TextStyle(fontSize: 120.0, color: Colors.black);
  @override
  Widget build(BuildContext context) {
    //Now we need multiple widgets into a parent = "Container" widget
    return new Scaffold(
      appBar: AppBar(
        elevation: 3.0,
        centerTitle: true,
        title: Text("KeyboardAction"),
      ),
        backgroundColor: Colors.lightBlue,
        body: new Center(
          child: new Text('$_screenTitle',
            style: _sizeTextWhite,
          ),

        )
    );
  }
}