import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/payTheDebtTrue.dart';


BuildContext _context;

class PayTheDebt extends StatefulWidget{
  @override
  _PayTheDebtState createState() => _PayTheDebtState();


}

class _PayTheDebtState extends State<PayTheDebt> {

  final myController = TextEditingController();

  bool _textIsPresent = false;


  void initState(){
    super.initState();
    myController.addListener(_changeButtonState);
  }

  _changeButtonState(){
    var text = myController.text.length > 0;
    if(text){
      setState(() => _textIsPresent = true);
    }else
      setState(() => _textIsPresent =false);
  }

  @override
  Widget build(BuildContext context) {

    _context = context;
    var _onPressed;

    if(_textIsPresent){
      _onPressed = ()=> Navigator.push(_context, MaterialPageRoute(builder: (context) => PayTheDebtTrue(int.parse(myController.text))));
    }

    double width = MediaQuery.of(context).size.width;
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          iconTheme: IconThemeData(color: Colors.black),
          title: new Text(
            'Погасити заборгованність',
            style: new TextStyle(color: Colors.black),
          ),
        ),
        body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Column(
                  children: <Widget>[
                    new Container(
                      decoration: new BoxDecoration(color: Colors.red),
                      child: Column(
                        children: <Widget>[
                          new Container(
                            decoration: BoxDecoration(color: Colors.white),
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Row(children: <Widget>[
                                  new Container(
                                      height: 65.0,
                                      decoration:
                                      BoxDecoration(color: Colors.black12),
                                      child: Row(
                                        children: <Widget>[
                                          new Container(
                                            width: width * 0.6,
                                            child: new Row(children: <Widget>[
                                              new Container(
                                                margin: const EdgeInsets.only(
                                                    right: 10.0, left: 16.0),
                                                child: CircleAvatar(
                                                  backgroundColor:
                                                  Color(0xFFEB3423),
                                                  child: new Image(
                                                      image: new AssetImage(
                                                          "res/img/alfa_icon.png")),
                                                ),
                                              ),
                                              new Container(
                                                child: new Column(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    new Container(
                                                      margin: const EdgeInsets.only(
                                                          bottom: 5.0),
                                                      child: new Text(
                                                        '1234 5678 9012 3456',
                                                        textAlign: TextAlign.left,
                                                        style: TextStyle(
                                                            fontSize: 12.0,
                                                            color:
                                                            Color(0xFF828282)),
                                                      ),
                                                    ),
                                                    new Container(
                                                      child: new Text(
                                                        'Кредитна картка',
                                                        style: new TextStyle(
                                                            fontSize: 18.0),
                                                        textAlign: TextAlign.start,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ]),
                                          ),
                                          new GestureDetector(
                                            onTap: () {
                                              Navigator.pop(context);
                                            },
                                            child: new Container(
                                                width: width * 0.4,
                                                child: new Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                        0.0, 0.0, 16.0, 0.0),
                                                    child: new Column(
                                                      mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                      children: <Widget>[
                                                        new Container(
                                                          height: 35.0,
                                                          child: new Row(
                                                            mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                            children: <Widget>[
                                                              new Text(
                                                                '2 282.1 ₴',
                                                                style: new TextStyle(
                                                                    color: Colors
                                                                        .lightGreen,
                                                                    fontSize: 18.0,
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                              ),
                                                              new Padding(
                                                                padding: EdgeInsets
                                                                    .fromLTRB(
                                                                    5.0,
                                                                    0.0,
                                                                    0.0,
                                                                    0.0),
                                                                child: new Icon(
                                                                    Icons.edit,
                                                                    color: Colors
                                                                        .black45),
                                                              )
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ))),
                                          )
                                        ],
                                      )),
                                ]),
                                new Row(
                                  children: <Widget>[
                                    new Container(
                                      height: 65.0,
                                      child: new Padding(
                                        padding: EdgeInsets.only(left: 16.0),
                                        child: new Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: <Widget>[
                                            new Row(
                                              children: <Widget>[
                                                new Text(
                                                  'Рекомендований платіж:',
                                                  style:
                                                  new TextStyle(fontSize: 18.0),
                                                  textAlign: TextAlign.start,
                                                ),
                                                new Padding(
                                                  padding:
                                                  EdgeInsets.only(left: 5.0),
                                                  child: new Text(
                                                    '800 ₴',
                                                    style: new TextStyle(
                                                        color: Colors.lightGreen,
                                                        fontSize: 20.0,
                                                        fontWeight:
                                                        FontWeight.bold),
                                                  ),
                                                )
                                              ],
                                            ),
                                            new Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.start,
                                              children: <Widget>[
                                                new Padding(
                                                  padding:
                                                  EdgeInsets.only(top: 5.0),
                                                  child: new Text(
                                                    'Сплатити до 21 листопада',
                                                    style: new TextStyle(
                                                        fontSize: 16.0,
                                                        color: Colors.grey),
                                                    textAlign: TextAlign.start,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                new Column(
                                  children: <Widget>[
                                    new Container(
                                      child: new Theme(
                                          data: new ThemeData(
                                            primaryColor: Colors.cyan,
                                            cursorColor: Colors.cyan,
                                            hintColor: Colors.grey,
                                            indicatorColor: Colors.amber,
                                          ),
                                          child: new TextFormField(
                                            controller: myController,

                                            keyboardType:
                                            TextInputType.numberWithOptions(),
                                            style: new TextStyle(
                                                color: Colors.black54,
                                                fontSize: 16.0),
                                            decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                borderSide: BorderSide(),
                                                borderRadius:
                                                BorderRadius.circular(5.0),
                                              ),
                                              labelText: 'Сумма платежа',
                                            ),
                                          )),
                                      padding: EdgeInsets.only(
                                          left: 16.0, right: 16.0, top: 16.0),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    new Container(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Padding(
                            padding: new EdgeInsets.all(50.0),
                            child: new SizedBox(
                              width: 250.0,
                              height: 48.0,
                              child: new RaisedButton(
                                onPressed: _onPressed,
                                shape: new RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(30.0)),
                                disabledColor: Color(0xFFeddfa3),
                                disabledTextColor: Colors.grey,
                                textColor: Colors.black,
                                color: Colors.yellow,
                                child: new Text('ПОГАСИТИ ЗАБОРГОВАННІСТЬ',
                                    style: new TextStyle(
                                      fontSize: 14.0, )),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )
              ],
            )
        ));
  }


}


