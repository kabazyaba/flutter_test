import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/myAccounts.dart';

class Invoice extends StatefulWidget {

    final String _money;
    final String _card;
//    const Invoice.anotherInvoice(this._card, this._money);
    const Invoice(this._money, this._card);



  @override
  _InvoiceState createState() => _InvoiceState();
}

class _InvoiceState extends State<Invoice> {



  bool _checkBoxVal = false;

  @override
  Widget build(BuildContext context) {

    double width = MediaQuery.of(context).size.width;


    return new WillPopScope(
        onWillPop: () async {
          if(_checkBoxVal) {
            return Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> MyAccounts()));
          } else
            return Navigator.pop(context);
        },


        child: new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          iconTheme: IconThemeData(color: Colors.black),
          title: new Text(
            'Квітанція',
            style: new TextStyle(color: Colors.black),
          ),
        ),
        body: Container(
            decoration: new BoxDecoration(color: Colors.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Column(
                  children: <Widget>[
                    new Divider(
                      color: Colors.grey,
                    ),
                    Column(
                      children: <Widget>[
                        new Container(
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Row(children: <Widget>[
                                new Container(
                                    height: 65.0,
                                    child: Row(
                                      children: <Widget>[
                                        new Container(
                                          child: new Row(children: <Widget>[
                                            new Container(
                                              margin: const EdgeInsets.only(
                                                  right: 16.0, left: 16.0),
                                              child: CircleAvatar(
                                                backgroundColor: Colors.grey,
                                              ),
                                            ),
                                            new Container(
                                              child: new Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  new Container(
                                                    width: width * 0.8,
                                                    child: new Text(
                                                      'Переказ ${widget._money} ₴ з картки ${widget._card} на картку *1234',
                                                      textAlign: TextAlign.left,
                                                      maxLines: 3,
                                                      style: TextStyle(
                                                          fontSize: 19.0,
                                                          color: Colors.black),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ]),
                                        ),
                                      ],
                                    )),
                              ]),
                              new Row(children: <Widget>[
                                new Container(
                                  margin: const EdgeInsets.only(
                                      right: 16.0, left: 16.0),
                                  child: CircleAvatar(
                                    backgroundColor: Colors.grey,
                                  ),
                                ),
                                new Container(
                                  child: new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Container(
                                        width: width * 0.8,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            new Text(
                                              'Залишок після операції',
                                              textAlign: TextAlign.left,
                                              maxLines: 3,
                                              style: TextStyle(
                                                  fontSize: 19.0,
                                                  color: Colors.black),
                                            ),
                                            new Text(
                                              '1090.0 ₴',
                                              textAlign: TextAlign.left,
                                              maxLines: 3,
                                              style: TextStyle(
                                                  fontSize: 19.0,
                                                  color: Colors.lightGreen),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ])
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                new Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Padding(
                            padding:
                                new EdgeInsets.only(bottom: 50.0, left: 16.0),
                            child: Row(
                              children: <Widget>[
                                new Checkbox(
                                    value: this._checkBoxVal,
                                    onChanged: (bool value) {
                                      setState(() => this._checkBoxVal = value);
                                    }),
                                Padding(
                                  padding: const EdgeInsets.only(left: 30.0),
                                  child: new Text(
                                    'Зберегти у шаблонах',
                                    style: new TextStyle(
                                        fontSize: 18.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),

                    new Row(
//                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(25.0),
                          child: new GestureDetector(
                            child: Container(
//                            height: 25.0,
//                            width: 50.0,
                              child: new Text('НАДІСЛАТИ НА EMAIL   OK',
                                style: new TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w700
                                ),

                              ),
                            ),
                          )

                        )
                      ],
                    )
                  ],
                )
              ],
            ))));
  }
}
