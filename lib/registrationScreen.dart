import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_groshi/myAccounts.dart';



BuildContext _context;

class RegistrationScreen extends StatelessWidget {

  final double _width = 300.0;


  @override
  Widget build(BuildContext context) {

    _context = context;
    return Scaffold(
        resizeToAvoidBottomPadding:false,
        backgroundColor: Color(0xFF137EB5),
        appBar: AppBar(
          title: Text("Реєстрація"),
        ),
        body: Center(
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Container(
                  width: _width,
                  child: new Theme(
                      data: new ThemeData(
                        primaryColor: Color(0xFFFFDD00),
                        cursorColor: Color(0xFFFFDD00),
                        hintColor: Colors.white54,
                        indicatorColor: Colors.amber,
                      ),
                      child: new TextFormField(
                        keyboardType: TextInputType.phone,
                        maxLength: 9,
                        maxLengthEnforced: true,
                        style: new TextStyle(color: Colors.white, fontSize: 16.0),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderSide: BorderSide(),
                            borderRadius:BorderRadius.circular(5.0),
                          ),
                          labelText: 'Телефон',
                          hintText: '+38(***)***-**-**',
                          prefixText: "\+380"
                        ),
                      )
                  ),
                  padding: EdgeInsets.only(left: 16.0,right: 16.0, top: 24.0),
                ),
                new Container(
                  width: _width,
                  child: new Theme(
                      data: new ThemeData(
                        primaryColor: Color(0xFFFFDD00),
                        cursorColor: Color(0xFFFFDD00),
                        hintColor: Colors.white54,
                        indicatorColor: Colors.amber,
                      ),
                      child: new TextFormField(
                        keyboardType: TextInputType.text,
                        style: new TextStyle(color: Colors.white, fontSize: 16.0),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderSide: BorderSide(),
                            borderRadius:BorderRadius.circular(5.0),
                          ),
                          labelText: 'SMS-код',
                        ),
                      )
                  ),
                  padding: EdgeInsets.only(left: 16.0,right: 16.0, top: 16.0),
                ),
                new Container(
                )
              ]
          ),
        ),
      floatingActionButton: new FloatingActionButton(
      backgroundColor: Color(0xFFFFDD00),
      onPressed: registrationAction,
      child: new Icon(Icons.arrow_forward),
    ),
    );
  }

  void registrationAction(){
    Navigator.push(_context, MaterialPageRoute(builder: (context) => MyAccounts()));
  }
}