import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/callbackOrder.dart';


class CreditCalc extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<CreditCalc> {

  double _valueMoney = 1000.0;
  double _valueMonth = 5.0;
  double _creditPayment = 0.0;

  void _onChangedMoney(double value) {
    setState(() {
      _valueMoney = value;
    });
  }

  void _onChangedMonth(double value) {
    setState(() {
      _valueMonth = value;
    });
  }

  @override
  Widget build(BuildContext context) {

    int _valueIntMoney = _valueMoney.round();
    int _valueIntMonth = _valueMonth.round();

    _creditPayment = double.parse((_valueIntMoney/_valueIntMonth + 100).toStringAsFixed(2));



    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          iconTheme: IconThemeData(color: Colors.black),
          title: new Text(
            'Кредитний калькулятор',
            style: new TextStyle(color: Colors.black),
          ),
        ),
        body: Container(
          decoration: new BoxDecoration(color: Colors.white),
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Column(
              children: <Widget>[
                new Divider(
                  color: Colors.grey,
                ),
                 Column(
                    children: <Widget>[
                      new Container(
                        decoration: BoxDecoration(color: Colors.white),
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new Row(children: <Widget>[
                              new Container(
                                  height: 190.0,
                                  width: MediaQuery.of(context).size.width,
                                  decoration:
                                      BoxDecoration(color: Colors.white),
                                  child: Column(
                                    children: <Widget>[
                                      new Padding(
                                        padding: EdgeInsets.only(
                                            left: 18.0,
                                            right: 18.0,
                                            top: 10.0,
                                            bottom: 5.0),
                                        child: new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            new Container(
                                              child: new Text(
                                                'Сума',
                                                style: new TextStyle(
                                                    color: Colors.grey),
                                              ),
                                            ),
                                            new Container(
                                              child: new Text('$_valueIntMoney ₴',
                                                  style: new TextStyle(
                                                      color: Colors.lightGreen,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 18.0)),
                                            )
                                          ],
                                        ),
                                      ),
                                      new Padding(
                                        padding: EdgeInsets.only(
                                            left: 10.0, right: 10.0),
                                        child: new Slider(
                                            min: 500.0,
                                            max: 20000.0,
                                            value: _valueMoney,
                                            divisions: 195,
                                            onChanged: (double value) {
                                              _onChangedMoney(value);
                                            }),
                                      ),

                                      new Padding(
                                        padding: EdgeInsets.only(
                                            left: 18.0,
                                            right: 18.0,
                                            top: 10.0,
                                            bottom: 5.0),
                                        child: new Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            new Container(
                                              child: new Text(
                                                'Строк',
                                                style: new TextStyle(
                                                    color: Colors.grey),
                                              ),
                                            ),
                                            new Container(
                                              child: new Text('$_valueIntMonth міс',
                                                  style: new TextStyle(
                                                      color: Colors.lightGreen,
                                                      fontWeight:
                                                      FontWeight.bold,
                                                      fontSize: 18.0)),
                                            )
                                          ],
                                        ),
                                      ),
                                      new Padding(
                                        padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 30.0),
                                        child: new Slider(
                                            min: 2.0,
                                            max: 36.0,
                                            value: _valueMonth,
                                            onChanged: (double value) {
                                              _onChangedMonth(value);
                                            }),
                                      ),
                                      new Divider(color: Colors.grey,)

                                    ],
                                  )),
                            ]),
                            new Row(
                              children: <Widget>[
                                new Container(
                                  height: 65.0,
                                  child: new Padding(
                                    padding: EdgeInsets.only(left: 16.0),
                                    child: new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Row(
                                          children: <Widget>[
                                            new Text(
                                              'Щомісячний платіж: ',
                                              style:
                                                  new TextStyle(fontSize: 18.0),
                                              textAlign: TextAlign.start,
                                            ),
                                            new Padding(
                                              padding:
                                                  EdgeInsets.only(left: 5.0),
                                              child: new Text(
                                                '$_creditPayment ₴ *',
                                                style: new TextStyle(
                                                    color: Colors.lightGreen,
                                                    fontSize: 20.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            )
                                          ],
                                        ),
                                        new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Padding(
                                              padding:
                                                  EdgeInsets.only(top: 5.0),
                                              child: new Text(
                                                '* Приблизне значення',
                                                style: new TextStyle(
                                                    fontSize: 16.0,
                                                    color: Colors.grey),
                                                textAlign: TextAlign.start,
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
              ],
            ),
            new Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                new Container(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      new Padding(
                        padding: new EdgeInsets.all(50.0),
                        child: new SizedBox(
                          width: 250.0,
                          height: 48.0,
                          child: new RaisedButton(
                            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => CallbackOrder())),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            disabledColor: Color(0xFFeddfa3),
                            disabledTextColor: Colors.grey,
                            textColor: Colors.black,
                            color: Colors.yellow,
                            child: new Text('ПОДАТИ ЗАЯВКУ',
                                style: new TextStyle(
                                  fontSize: 14.0,
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            )
          ],
        )));
  }
}
