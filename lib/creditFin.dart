import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/myAccounts.dart';



class CreditFin extends StatefulWidget {
  @override
  _mTestState createState() => _mTestState();
}

class _mTestState extends State<CreditFin> {

  initState() {
    super.initState();
    new Timer(const Duration(seconds: 4), onClose);
  }

  void onClose() {
    if (mounted) {
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyAccounts()));
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return new Scaffold(
        body: Container(
          decoration: new BoxDecoration(color: Colors.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Padding(
                      padding: EdgeInsets.only(bottom: 25.0),
                      child: new Icon(
                        Icons.thumb_up,
                        size: 80.0,
                        color: Colors.cyan,
                      ),
                    ),
                    new Padding(
                      padding: EdgeInsets.only(left: 20.0, right: 20.0),
                      child: new Text(
                        'Очікуйте на дзвінок оператора. Дякуємо, що довіряєте нам.',
                        textAlign: TextAlign.center,
                        style: new TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.black
                        ),
                      ),
                    ),
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new SizedBox(
                          height: 35.0,
                        ),
                      ],
                    )
                  ],
                ),
              ],
            )));
  }
}
