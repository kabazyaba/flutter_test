import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/keyboardAction.dart';
import 'package:flutter_app_groshi/main.dart';


class Test2 extends StatefulWidget {
  Test2({Key key, this.title}) :super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<Test2> {


  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: () async => Navigator
          .of(context)
          .pushReplacement(new MaterialPageRoute(builder: (BuildContext context) => KeyboardAction('1'))),
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text("data"),
          leading: new IconButton(
            icon: new Icon(Icons.ac_unit),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
      ),
    );
  }
}