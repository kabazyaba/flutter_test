import 'package:flutter/material.dart';
import 'package:flutter_app_groshi/CustomShowDialog.dart';
import 'package:flutter_app_groshi/accountDetails.dart';
import 'package:flutter_app_groshi/autorizationScreen.dart';
import 'package:flutter_app_groshi/payTheDebt.dart';
import 'package:flutter_app_groshi/payTheDebt.dart';

class Dialogs {
  information(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return CustomAlertDialog(
            contentPadding: EdgeInsets.only(left: 0.0, top: 20.0, right: 0.0, bottom: 8.0),
            title: Text('Погасити заборгованість', style: new TextStyle(fontSize: 20.0)),
            content: SingleChildScrollView(
              child: new Column(
                children: <Widget>[
                  new Divider(
                    color: Colors.black26,
                    height: 8.0,
                  ),

//Alfa bank button

                  new GestureDetector(
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PayTheDebt()),
                      );
                    },
                    child: new Container(
                        margin: EdgeInsets.only(left: 24.0, top: 8.0, right: 24.0, bottom: 8.0),
                        child: new Row(
                          children: <Widget>[
                            new Container(
                              margin: const EdgeInsets.only( right: 20.0),
                              child: CircleAvatar(
                                backgroundColor: Color(0xFFEB3423),
                                child: new Image(image: new AssetImage("res/img/alfa_icon.png")
                                ),
                              ),
                            ),
                            new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    child: new Text('1234 5678 9012 3456',
                                      textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0, color: Color(0xFF828282)),
                                    ),
                                  ),
                                  new Container(
                                    child: new Text('2 282.1 ₴',
                                      textAlign: TextAlign.start,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        )
                    ),
                  ),

                  new Divider(
                    color: Colors.black26,
                    height: 8.0,
                  ),


// OTP bank button

                  new GestureDetector(
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => AuthorizationScreen()),
                      );
                    },
                    child: new Container(
                        margin: EdgeInsets.only(left: 24.0, top: 8.0, right: 24.0, bottom: 8.0),
                        child: new Row(
                          children: <Widget>[
                            new Container(
                              margin: const EdgeInsets.only( right: 20.0),
                              child: CircleAvatar(
                                backgroundColor: Color(0xFF7AC042),
                                child: new Image(image: new AssetImage("res/img/otp_icon.png")
                                ),
                              ),
                            ),
                            new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    child: new Text('1234 5678 9012 3456',
                                      textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0, color: Color(0xFF828282)),
                                    ),
                                  ),
                                  new Container(
                                    child: new Text('2 282.1 ₴',
                                      textAlign: TextAlign.start,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        )
                    ),
                  ),

                  new Divider(
                    color: Colors.black26,
                    height: 8.0,
                  ),


// Add new card

                  new GestureDetector(
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => AuthorizationScreen()),
                      );
                    },
                    child: new Container(
                        margin: EdgeInsets.only(left: 24.0, top: 8.0, right: 24.0, bottom: 8.0),
                        child: new Row(
                          children: <Widget>[
                            new Container(
                              margin: const EdgeInsets.only( right: 20.0),
                              child: CircleAvatar(
                                backgroundColor: Color(0xFF707070),
                                child: new Image(image: new AssetImage("res/img/custom_card_icon.png")
                                ),
                              ),
                            ),
                            new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    child: new Text('Ввести вручну',
                                      textAlign: TextAlign.start,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        )
                    ),
                  ),









                ],
              ),
            ),
// actions: <Widget>[
// FlatButton(
// onPressed: () => Navigator.pop(context),
// child: Text('ok'),
// )
// ],
          );
        }
    );
  }

}